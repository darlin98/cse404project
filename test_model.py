import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import os
from PIL import Image
import xml.etree.ElementTree as ET

# function pad all image arrays to same size
def pad_image(image, target=(600,600)):
    pad_height = max(target[0] - image.shape[0], 0)  # Padding for height
    pad_width = max(target[1] - image.shape[1], 0)  # Padding for width

    padding = ((0, pad_height), (0, pad_width), (0, 0)) if image.ndim == 3 else ((0, pad_height), (0, pad_width))

    padded_image = np.pad(image, padding, mode='constant', constant_values=0)
    return padded_image

def load_data(images, labels):
    for image, label in zip(images, labels):
        yield image, label

def main():
    bounding_boxes = []
    valid_filenames = []  # List to hold filenames with a single bounding box
    # Loop over all XML files in the directory
    for filename in os.listdir('plate_data/annotations'):
        if filename.endswith(".xml"):  # Check for file extension
            xml_path = os.path.join('plate_data/annotations', filename)
            tree = ET.parse(xml_path)
            root = tree.getroot()
            # Check if there's exactly one object tag
            objects = root.findall('object')
            if len(objects) == 1:
                object_tag = objects[0]
                bndbox = object_tag.find('bndbox')
                xmin = int(bndbox.find('xmin').text)
                ymin = int(bndbox.find('ymin').text)
                xmax = int(bndbox.find('xmax').text)
                ymax = int(bndbox.find('ymax').text)
                bounding_boxes.append([xmin, ymin, xmax, ymax])
                valid_filenames.append(root.find('filename').text)
    bounding_boxes = np.array(bounding_boxes)

    images = []
    for filename in valid_filenames:
        img_path = os.path.join('plate_data/images', filename)
        if os.path.exists(img_path):  # Make sure the file exists
            img = Image.open(img_path).convert('L')
            img_array = np.array(img)
            images.append(img_array)

    # add padding to all the images
    for i in range(len(images)):
        images[i] = pad_image(images[i])

    images = np.array(images)
    images = np.expand_dims(images, -1)
    print(images.shape)

    # normalize pixel values
    images = images / 255

    # normalize bounding box values
    bounding_boxes = bounding_boxes / 600

    # load a saved model
    from tensorflow.keras.models import load_model
    model = load_model('trained_model/plate_recognizer_01.h5')

    # test the trained model
    test_index = 46

    test_image = np.expand_dims(images[test_index], axis=0)
    predicted_box = model.predict(test_image)

    predicted_box = np.squeeze(predicted_box, axis=0)

    predicted_box = predicted_box * 600
    test_image = test_image * 255
    #predicted_box[0] = predicted_box[0] / 200.0
    #predicted_box[0][3] = predicted_box[0][3] * -1.0
    real_box = bounding_boxes[test_index] * 600

    print(predicted_box)
    print(real_box)

    # print(images_test.shape)
    # print(boxes_test.shape)
    fig, ax = plt.subplots(1)

    # Display the original image
    plt.imshow(np.squeeze(test_image, axis=0).astype('float32'), cmap='gray')  # Assuming this is already normalized and ready for display

    # Create a Rectangle patch for the predicted bounding box
    rect = patches.Rectangle((predicted_box[0], predicted_box[1]), predicted_box[2]-predicted_box[0], predicted_box[3]-predicted_box[1], linewidth=1, edgecolor='r', facecolor='none')
    ax.add_patch(rect)

    plt.axis('off')  # Hide the axis
    plt.show()



if __name__ == "__main__":
    main()