import os
from PIL import Image
import numpy as np
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from sklearn.model_selection import train_test_split
import tensorflow as tf
from tensorflow.keras import layers, models
from tensorflow.keras.mixed_precision import set_global_policy

set_global_policy('mixed_float16')

# function pad all image arrays to same size
def pad_image(image, target=(600,600)):
    pad_height = max(target[0] - image.shape[0], 0)  # Padding for height
    pad_width = max(target[1] - image.shape[1], 0)  # Padding for width

    padding = ((0, pad_height), (0, pad_width), (0, 0)) if image.ndim == 3 else ((0, pad_height), (0, pad_width))

    padded_image = np.pad(image, padding, mode='constant', constant_values=0)
    return padded_image

def load_data(images, labels):
    for image, label in zip(images, labels):
        yield image, label

def main():
    bounding_boxes = []
    valid_filenames = []  # List to hold filenames with a single bounding box
    # Loop over all XML files in the directory
    for filename in os.listdir('plate_data/annotations'):
        if filename.endswith(".xml"):  # Check for file extension
            xml_path = os.path.join('plate_data/annotations', filename)
            tree = ET.parse(xml_path)
            root = tree.getroot()
            # Check if there's exactly one object tag
            objects = root.findall('object')
            if len(objects) == 1:
                object_tag = objects[0]
                bndbox = object_tag.find('bndbox')
                xmin = int(bndbox.find('xmin').text)
                ymin = int(bndbox.find('ymin').text)
                xmax = int(bndbox.find('xmax').text)
                ymax = int(bndbox.find('ymax').text)
                bounding_boxes.append([xmin, ymin, xmax, ymax])
                valid_filenames.append(root.find('filename').text)
    bounding_boxes = np.array(bounding_boxes)

    images = []
    for filename in valid_filenames:
        img_path = os.path.join('plate_data/images', filename)
        if os.path.exists(img_path):  # Make sure the file exists
            img = Image.open(img_path).convert('L')
            img_array = np.array(img)
            images.append(img_array)

    # add padding to all the images
    for i in range(len(images)):
        images[i] = pad_image(images[i])

    images = np.array(images)
    images = np.expand_dims(images, -1)
    print(images.shape)

    # normalize pixel values
    images = images / 255

    # normalize bounding box values
    bounding_boxes = bounding_boxes / 600

    images_train, images_val, boxes_train, boxes_val = train_test_split(
    images, bounding_boxes, test_size=0.25, random_state=42)

    BATCH_SIZE = 32
    train_dataset = tf.data.Dataset.from_generator(
        lambda: load_data(images_train, boxes_train),
        output_types=(images_train.dtype, boxes_train.dtype),
        output_shapes=([600, 600,1], [4])
    )

    validation_dataset = tf.data.Dataset.from_generator(
        lambda: load_data(images_val, boxes_val),
        output_types=(images_val.dtype, boxes_val.dtype),
        output_shapes=([600, 600,1], [4])
    )

    # Batch the datasets
    train_dataset = train_dataset.batch(BATCH_SIZE).prefetch(tf.data.AUTOTUNE)
    validation_dataset = validation_dataset.batch(BATCH_SIZE).prefetch(tf.data.AUTOTUNE)

    # create the cnn model
    model = tf.keras.Sequential([
        # First convolutional block
        layers.Conv2D(64, (3, 3), activation='relu', input_shape=(600, 600, 1), padding='same'),
        layers.BatchNormalization(),
        layers.MaxPooling2D((2, 2)),
        layers.Dropout(0.01),

        # Second convolutional block
        layers.Conv2D(128, (3, 3), activation='relu'),
        layers.BatchNormalization(),
        layers.MaxPooling2D((2, 2)),
        layers.Dropout(0.05),

        # Third convolutional block
        layers.Conv2D(128, (3, 3), activation='relu'),
        layers.BatchNormalization(),
        layers.MaxPooling2D((2, 2)),
        layers.Dropout(0.05),

        # Third convolutional block
        layers.Conv2D(64, (3, 3), activation='relu'),
        layers.BatchNormalization(),
        layers.MaxPooling2D((2, 2)),
        layers.Dropout(0.05),

        layers.Flatten(),
        
        # Fully connected layers
        layers.Dense(512, activation='relu'),
        layers.Dropout(0.2),

        # Output layer with 5 units (for the bounding box [confidence, x, y, size_x, size_y])
        layers.Dense(4),
        #CustomOutputLayer()
    ])

    # compile the model
    model.compile(optimizer='adam', loss='mse', metrics=['mae'])

    history = model.fit(
        train_dataset,  # Training data and labels
        epochs=30,  # Number of epochs to train for
        validation_data=validation_dataset  # Validation data and labels
    )

    # save the trained model
    model.save('trained_model/plate_recognizer_01.h5')

if __name__ == "__main__":
    main()

    